public class Calculations
{
  public double calculate(Triangle t)
  {
    if (t.getA() > 0 && t.getB() > 0)
    {
      return Math.sqrt(Math.pow(t.getA(), 2) + Math.pow(t.getB(), 2));
    }
    else
    {
      return 0.0;
    }
  }
}
