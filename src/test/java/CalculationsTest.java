import org.testng.Assert;
import org.testng.annotations.Test;

import static org.testng.Assert.*;

public class CalculationsTest
{
  @Test
  public void calculateZero()
  {
    Triangle t = new Triangle();
    t.setA(0);
    t.setB(0);
    Assert.assertEquals(calculations.calculate(t), 0.0);
  }

  @Test
  public void calculatePositiveNumbers()
  {
    Triangle t = new Triangle();
    t.setA(30);
    t.setB(40);

    Assert.assertEquals(calculations.calculate(t), 50.0);
  }

  @Test
  public void calculateNegativeNumber()
  {
    Triangle t = new Triangle();
    t.setA(-30);
    t.setB(40);

    Assert.assertEquals(calculations.calculate(t), 0.0);
  }

  @Test
  public void sideAInitialised()
  {
    Assert.assertEquals(triangle.getA(), 0);
  }

  @Test
  public void sideBInitialised()
  {
    Assert.assertEquals(triangle.getB(), 0);
  }

  @Test
  public void sideCInitialised()
  {
    Assert.assertEquals(triangle.getC(), 0);
  }

  Calculations calculations = new Calculations();
  Triangle triangle = new Triangle();
}